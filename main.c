#include "mem.h"

#include <stdio.h>
#include <string.h>


int main() 
{
    //printf("%ld\n", sizeof(struct mem));
    void * heap_start = NULL;
    printf("malloc1\n");
	struct mem* str = _malloc(4096);
    heap_start = (char*) str - sizeof(struct mem);
    memalloc_debug_heap(stdout,heap_start);
    printf("malloc2\n");
    char* str1 = _malloc(5);
    strcpy(str1, "1234");
    memalloc_debug_heap(stdout,heap_start);
    printf("free1\n");
    _free(str1);
    memalloc_debug_heap(stdout,heap_start);
    // printf("free2\n");
    // _free(str);
    // memalloc_debug_heap(stdout,heap_start);
}
