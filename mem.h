#ifndef MEM_H
#define MEM_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

#define HEAP_START ((void*)0x04040000)
#define MEMORY_CHUNK 4 * 1024 

#define DEBUG_FIRST_BYTES 4
#define LOG

struct mem;

#pragma pack(push, 1)
struct mem {
    struct mem* next;
    size_t capacity;
    bool is_free;
};
#pragma pack(pop)

void* _malloc(size_t query);
void  _free(void* mem);
void* heap_init(size_t initial_size);


void memalloc_debug_struct_info(FILE* f, struct mem const* const address);
void memalloc_debug_heap(FILE* f, struct mem const* ptr);


#endif //MEM_H
